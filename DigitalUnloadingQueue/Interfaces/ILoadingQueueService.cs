﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FrontOrder = DigitalUnloadingQueue.Models.LoadingOrder;

namespace DigitalUnloadingQueue.Interfaces
{
    public interface ILoadingQueueService
    {
        Task<Guid> AddOrder(FrontOrder loadingOrder);
        Task<bool> DeleteOrder(Guid orderId);
        Task<bool> PinOrderToGate(Guid orderId, Guid gateId, DateTime startTime, DateTime endTime,
            int processDuration);
        Task<bool> UnpinOrderFromGate(Guid orderId);
        Task<FrontOrder> CheckOrderState(Guid orderId);
        Task<FrontOrder> RecoverOrderByTruckName(string truckId);
        Task<List<FrontOrder>> GetItemsAsync();
        Task ChangeOrderInfo(Guid orderId, FrontOrder frontOrder);
    }
}