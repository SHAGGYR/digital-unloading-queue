using Blazored.Modal;
using DigitalUnloadingQueue.BussinessLogic;
using DigitalUnloadingQueue.BussinessLogic.Interfaces;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Contracts.Models;
using DigitalUnloadingQueue.Data;
using DigitalUnloadingQueue.DataAccess.Repositories;
using DigitalUnloadingQueue.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IWarehouseService = DigitalUnloadingQueue.Interfaces.IWarehouseService;
using WarehouseService = DigitalUnloadingQueue.Data.WarehouseService;

namespace DigitalUnloadingQueue
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddBlazoredModal();
            services.AddSingleton<WeatherForecastService>();
            services.AddSingleton<LoadingQueueService>();
            services.AddSingleton<GatesInfosInfoService>();
            services.AddSingleton<BaseDataAccess>();

            services.AddSingleton<IWarehouseRepository, WarehouseRepository>();
            services.AddSingleton<IOrderRepository, OrderRepository>();
            services.AddSingleton<IGateRepository, GateRepository>();

            services.AddSingleton<IWarehouseService, WarehouseService>();
            services.AddSingleton<ILoadingQueueService, LoadingQueueService>();
            services.AddSingleton<IGatesInfoService, GatesInfosInfoService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
