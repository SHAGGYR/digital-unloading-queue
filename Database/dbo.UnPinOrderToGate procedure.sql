USE [Hackaton]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[UnPinOrderToGate]
	@OrderIds NVARCHAR( MAX )
AS
BEGIN
	SET NOCOUNT ON;

		INSERT INTO Hackaton.dbo.QueueOrders(Id, DriverName, TruckId, PhoneNumber, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, ProcessTimeDuration)
		SELECT Id, DriverName, TruckId, PhoneNumber, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, ProcessTimeDuration
		FROM Hackaton.dbo.ProcessedOrders
		INNER JOIN String_Split( @OrderIds, N',' ) AS list ON list.[value] = Id;
		
		SELECT @@ROWCOUNT;

		DELETE ps FROM Hackaton.dbo.ProcessedOrders ps
		INNER JOIN String_Split( @OrderIds, N',' ) AS list ON list.[value] = ps.Id
END