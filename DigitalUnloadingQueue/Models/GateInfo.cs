﻿using System;
using System.Collections.Generic;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.Models
{
    public class GateInfo
    {
        public Guid GateId { get; set; } 
        public Guid WarehouseId { get; set; } = new Guid("A2862E94-2EF4-4193-B776-3BA2424B0B3C");
        public int GateNumber { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        
        public int AvailableLoadingTypes { get; set; }
        public List<PinnedOrder> PinnedOrders { get; set; } = new List<PinnedOrder>();
    }
}