﻿namespace DigitalUnloadingQueue.Contracts.Enums
{
    public enum OrderState
    {
        Scheduled = 1,
        InProgress = 2,
        Done = 3
    }
}