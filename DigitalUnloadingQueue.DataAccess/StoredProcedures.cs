﻿namespace DigitalUnloadingQueue.DataAccess
{
    public static class StoredProcedures
    {
        public const string AddWarehouse = "dbo.AddWarehouse";    
        public const string GetWarehouseInfo = "dbo.GetWarehouseInfo";    
        public const string DeleteWarehouse = "dbo.DeleteWarehouse";    
        public const string ChangeWarehouseInfo = "dbo.ChangeWarehouseInfo";    
        
        public const string AddLoadingOrder = "dbo.AddQueueOrder";   
        public const string DeleteOrderFromQueue = "dbo.DeleteQueueOrder";   
        public const string GetQueueOrderInfo = "dbo.GetOrderInfo";   
        public const string GetAllQueueOrders = "dbo.GetAllQueueOrders";   
        public const string ChangeQueueOrderInfo = "dbo.ChangeQueueOrderInfo";    
        public const string PinOrderToGate = "dbo.PinOrderToGate";    
        public const string UnpinOrderFromGate = "dbo.UnpinOrderFromGate";    

        
        public const string AddGate = "dbo.AddGate";
        public const string DeleteGate = "dbo.DeleteGate";
        public const string GetGateInfo = "dbo.GetGateInfo";
        public const string RecoverOrderByTruckName = "dbo.RecoverOrderByTruckName";
        public const string GetAllPinnedOrders = "dbo.GetAllPinnedOrders";
        public const string GetAllGatesInfo = "dbo.GetAllGatesInfo";
        public const string ChangeGateInfo = "dbo.ChangeGateInfo";
    }
}