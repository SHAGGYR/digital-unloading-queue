﻿using System;

namespace DigitalUnloadingQueue.Models
{
    public class Warehouse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime DefaultWorkTimeStart { get; set; }
        public DateTime DefaultWorkTimeStop { get; set; }
    }
}