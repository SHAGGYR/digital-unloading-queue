﻿using System;
using DigitalUnloadingQueue.Contracts.Enums;

namespace DigitalUnloadingQueue.Contracts.Models
{
    public class PinnedOrder: BaseOrder
    {
        public Guid GateId { get; set; }
        public DateTime ProcessTimeStart { get; set; }
        public DateTime ProcessTimeStop { get; set; }
        public OrderState CurrentState { get; set; }
    }
}