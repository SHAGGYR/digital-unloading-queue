SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.ChangeQueueOrderInfo
	@Id uniqueidentifier,
	@DriverName varchar(255),
	@PhoneNumber varchar(255),
	@TruckId varchar(255),
	@LoadName varchar(255),
	@Weight varchar(255),
	@Volume varchar(255),
	@BodyType varchar(255),
	@UnloadingType varchar(255),
	@Pack varchar(255),
	@Pieces varchar(255),
	@Belts varchar(255),
	@Dimensions varchar(255),
	@ADR varchar(255),
	@DateAdded datetime,
	@ProcessTimeDuration int
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE dbo.QueueOrders SET DriverName = @DriverName, PhoneNumber = @PhoneNumber, TruckId = @TruckId, LoadName = @LoadName, Weight = @Weight, 
	Volume = @Volume, BodyType = @BodyType, UnloadingType = @UnloadingType, Pack = @Pack, Pieces = @Pieces, Belts = @Belts, Dimensions = @Dimensions, ADR = @ADR, DateAdded = @DateAdded, ProcessTimeDuration = @ProcessTimeDuration Where Id = @Id;
	SELECT @@ROWCOUNT;
END
GO