﻿using System;

namespace DigitalUnloadingQueue.Models
{
    public class TruckInfo
    {
        public Guid OrderId { get; set; }
        public string CarId { get; set; }
        public int CarLoadingId { get; set; }
        public string DriverName { get; set; }
        public string Company { get; set; }
        public string ContactNumber { get; set; }
    }
}