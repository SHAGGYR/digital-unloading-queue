﻿using System;

namespace DigitalUnloadingQueue.Contracts.Enums
{
    [Flags]
    public enum CarLoadings
    {
        First = 1,
        Second = 2
    }
}