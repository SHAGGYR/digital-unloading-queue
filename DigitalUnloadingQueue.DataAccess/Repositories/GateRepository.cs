﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.DataAccess.Repositories
{
    public class GateRepository: IGateRepository
    {
        private readonly BaseDataAccess _dataAccess;

        public GateRepository( BaseDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public async Task<Guid> AddGate(Gate gate)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@WarehouseId", gate.WarehouseId);
            parameters.Add("@UniqueWorkTimeStart", gate.UniqueWorkTimeStart);
            parameters.Add("@UniqueWorkTimeStop", gate.UniqueWorkTimeStop);
            parameters.Add("@AvailableLoadingTypes", gate.AvailableLoadingTypes);
            parameters.Add("@GateNumber", gate.GateNumber);
            return await _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Guid>(StoredProcedures.AddGate, parameters);
        }

        public Task<Guid> DeleteGate(Guid gateId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", gateId);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Guid>(StoredProcedures.DeleteGate, parameters);
        }

        public Task<Gate> GetGateInfo(Guid gateId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", gateId);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Gate>(StoredProcedures.GetGateInfo, parameters);
        }

        public Task<IEnumerable<Gate>> GetAllGatesInfo()
        {
            return _dataAccess.ExecuteStoredProcedureWhichReturnsCollectionAsync<Gate>(StoredProcedures
                .GetAllGatesInfo);
        }
        
        public Task<IEnumerable<PinnedOrder>> GetAllPinnedOrders(Guid gateId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", gateId);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsCollectionAsync<PinnedOrder>(StoredProcedures
                .GetAllPinnedOrders, parameters);
        }

        public async Task<int> ChangeGateInfo(Guid gateId, Gate newGate)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", gateId);
            parameters.Add("@WarehouseId", newGate.WarehouseId);
            parameters.Add("@UniqueWorkTimeStart", newGate.UniqueWorkTimeStart);
            parameters.Add("@UniqueWorkTimeStop", newGate.UniqueWorkTimeStop);
            parameters.Add("@AvailableLoadingTypes", newGate.AvailableLoadingTypes);
            parameters.Add("@GateNumber", newGate.GateNumber);
            return await _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<int>(StoredProcedures.ChangeGateInfo, parameters);
        }
    }
}