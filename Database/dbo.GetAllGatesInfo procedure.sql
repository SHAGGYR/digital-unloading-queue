SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.GetAllGatesInfo
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, WarehouseId, UniqueWorkTimeStart, UniqueWorkTimeStop, AvailableLoadingTypes, GateNumber FROM dbo.Gates;
END
GO