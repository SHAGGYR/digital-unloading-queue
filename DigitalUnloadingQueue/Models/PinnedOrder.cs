﻿using System;
using DigitalUnloadingQueue.Contracts.Enums;

namespace DigitalUnloadingQueue.Models
{
    public class PinnedOrder: Order
    {
        public DateTime ProcessTimeStart { get; set; }
        
        public DateTime ProcessTimeStop { get; set; }
        
        public OrderState CurrentState { get; set; }
        
        public Guid GateId { get; set; }
    }
}