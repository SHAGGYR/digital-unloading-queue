SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.PinOrderToGate
	@OrderId uniqueidentifier,
	@GateId uniqueidentifier,
	@StartTime datetime,
	@EndTime datetime,
	@ProcessDuration int,
	@CurrentState int
AS
BEGIN
	SET NOCOUNT ON;

		INSERT INTO Hackaton.dbo.ProcessedOrders(Id, DriverName, TruckId, PhoneNumber, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, GateId, ProcessTimeStart, ProcessTimeStop, ProcessTimeDuration, CurrentState)
		SELECT Id, DriverName, TruckId, PhoneNumber, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, @GateId, @StartTime, @EndTime, @ProcessDuration, @CurrentState FROM Hackaton.dbo.QueueOrders WHERE Id = @OrderId;
		
		SELECT @@ROWCOUNT;

		DELETE FROM Hackaton.dbo.QueueOrders OUTPUT DELETED.Id WHERE Id = @OrderId;
END
GO