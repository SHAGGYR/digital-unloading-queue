﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.Contracts.Interfaces
{
    public interface IOrderRepository
    {
        Task<Guid> AddOrder(LoadingOrder loadingOrder);
        Task<Guid> DeleteQueueOrder(Guid orderId);
        Task<int> PinOrderToGate(Guid orderId, Guid gateId, DateTime startTime, DateTime endTime, int processDuration);
        Task<int> UnpinOrderFromGate(Guid orderId);
        Task<LoadingOrder> GetOrderInfo(Guid orderId);
        Task<int> ChangeOrderInfo(Guid orderInfo, LoadingOrder newLoadingOrder);
        Task<LoadingOrder> RecoverOrderByTruckName(string truckId);
        Task<IEnumerable<LoadingOrder>> GetAllOrdersInQueue();
    }
}