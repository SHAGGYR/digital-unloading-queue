SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.ChangeGateInfo
	@Id uniqueidentifier,
	@WarehouseId varchar(255),
	@UniqueWorkTimeStart datetime,
	@UniqueWorkTimeStop datetime,
	@AvailableLoadingTypes int,
	@GateNumber int
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE dbo.Gates SET WarehouseId = @WarehouseId, UniqueWorkTimeStart = @UniqueWorkTimeStart, UniqueWorkTimeStop = @UniqueWorkTimeStop, AvailableLoadingTypes = @AvailableLoadingTypes, GateNumber = @GateNumber Where Id = @Id;
	SELECT @@ROWCOUNT;
END
GO