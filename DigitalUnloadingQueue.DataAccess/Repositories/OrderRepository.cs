﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using DigitalUnloadingQueue.Contracts.Enums;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.DataAccess.Repositories
{
    public class OrderRepository: IOrderRepository
    {
        private readonly BaseDataAccess _dataAccess;

        public OrderRepository(BaseDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public Task<Guid> AddOrder(LoadingOrder loadingOrder)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@DriverName", loadingOrder.DriverName);
            parameters.Add("@PhoneNumber", loadingOrder.PhoneNumber);
            parameters.Add("@TruckId", loadingOrder.TruckId);
            parameters.Add("@LoadName", loadingOrder.LoadName);
            parameters.Add("@Weight", loadingOrder.Weight);
            parameters.Add("@Volume", loadingOrder.Volume);
            parameters.Add("@BodyType", loadingOrder.BodyType);
            parameters.Add("@UnloadingType", loadingOrder.UnloadingType);
            parameters.Add("@Pack", loadingOrder.Pack);
            parameters.Add("@Pieces", loadingOrder.Pieces);
            parameters.Add("@Belts", loadingOrder.Belts);
            parameters.Add("@Dimensions", loadingOrder.Dimensions);
            parameters.Add("@ADR", loadingOrder.ADR);
            parameters.Add("@DateAdded", loadingOrder.DateAdded);
            parameters.Add("@ProcessTimeDuration", loadingOrder.ProcessTimeDuration);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Guid>(StoredProcedures.AddLoadingOrder, parameters);
        }

        public Task<Guid> DeleteQueueOrder(Guid orderId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", orderId);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Guid>(StoredProcedures.DeleteOrderFromQueue, parameters);
        }

        public Task<int> PinOrderToGate(Guid orderId, Guid gateId, DateTime startTime, DateTime endTime, int processDuration)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@OrderId", orderId);
            parameters.Add("@GateId", gateId);
            parameters.Add("@StartTime", startTime);
            parameters.Add("@EndTime", endTime);
            parameters.Add("@ProcessDuration", processDuration);
            parameters.Add("@CurrentState", (int)OrderState.Scheduled);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<int>(StoredProcedures.PinOrderToGate, parameters);
        }

        public Task<int> UnpinOrderFromGate(Guid orderId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@OrderIds", orderId.ToString());
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<int>(StoredProcedures.UnpinOrderFromGate, parameters);
        }

        public Task<LoadingOrder> GetOrderInfo(Guid orderId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", orderId);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<LoadingOrder>(StoredProcedures.GetQueueOrderInfo, parameters);
        }

        public Task<int> ChangeOrderInfo(Guid orderId, LoadingOrder newLoadingOrder)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", orderId);
            parameters.Add("@DriverName", newLoadingOrder.DriverName);
            parameters.Add("@PhoneNumber", newLoadingOrder.PhoneNumber);
            parameters.Add("@TruckId", newLoadingOrder.TruckId);
            parameters.Add("@LoadName", newLoadingOrder.LoadName);
            parameters.Add("@Weight", newLoadingOrder.Weight);
            parameters.Add("@Volume", newLoadingOrder.Volume);
            parameters.Add("@BodyType", newLoadingOrder.BodyType);
            parameters.Add("@UnloadingType", newLoadingOrder.UnloadingType);
            parameters.Add("@Pack", newLoadingOrder.Pack);
            parameters.Add("@Pieces", newLoadingOrder.Pieces);
            parameters.Add("@Belts", newLoadingOrder.Belts);
            parameters.Add("@Dimensions", newLoadingOrder.Dimensions);
            parameters.Add("@ADR", newLoadingOrder.ADR);
            parameters.Add("@DateAdded", newLoadingOrder.DateAdded);
            parameters.Add("@ProcessTimeDuration", newLoadingOrder.ProcessTimeDuration);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<int>(StoredProcedures.ChangeQueueOrderInfo, parameters);
        }

        public Task<LoadingOrder> RecoverOrderByTruckName(string truckId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@TruckId", truckId);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<LoadingOrder>(StoredProcedures.RecoverOrderByTruckName,
                parameters);
        }

        public Task<IEnumerable<LoadingOrder>> GetAllOrdersInQueue()
        {
            return _dataAccess.ExecuteStoredProcedureWhichReturnsCollectionAsync<LoadingOrder>(StoredProcedures
                .GetAllQueueOrders);
        }
    }
}