SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.GetAllPinnedOrders
	@Id uniqueidentifier

AS
BEGIN
	SET NOCOUNT ON;

	SELECT Orders.Id, DriverName, PhoneNumber, TruckId, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, ProcessTimeStart, ProcessTimeStop, ProcessTimeDuration, GateId, CurrentState
	FROM Hackaton.dbo.Gates Gates 
	INNER JOIN Hackaton.dbo.ProcessedOrders Orders ON Gates.Id = Orders.GateId
	WHERE Gates.Id  = @Id;
END
GO
