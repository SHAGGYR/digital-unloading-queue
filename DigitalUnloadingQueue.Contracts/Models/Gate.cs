﻿using System;
using DigitalUnloadingQueue.Contracts.Enums;

namespace DigitalUnloadingQueue.Contracts.Models
{
    public class Gate
    {
        public Guid Id { get; set; }
        public int GateNumber { get; set; }
        public Guid WarehouseId { get; set; }
        public DateTime UniqueWorkTimeStart { get; set; }
        public DateTime UniqueWorkTimeStop { get; set; }
        public int AvailableLoadingTypes { get; set; }
    }
}