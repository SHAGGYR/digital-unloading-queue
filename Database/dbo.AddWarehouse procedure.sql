SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.AddWarehouse
	@Name varchar(255),
	@Location varchar(255),
	@DefaultWorkTimeStart varchar(255),
	@DefaultWorkTimeStop varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.Warehouses (Name, Location, DefaultWorkTimeStart, DefaultWorkTimeStop)
	OUTPUT INSERTED.Id
	VALUES (@Name, @Location, @DefaultWorkTimeStart, @DefaultWorkTimeStop)
END
GO