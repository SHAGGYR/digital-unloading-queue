﻿using System;
using System.Threading.Tasks;
using FrontWareHouse = DigitalUnloadingQueue.Models.Warehouse;

namespace DigitalUnloadingQueue.Interfaces
{
    public interface IWarehouseService
    {
        Task<Guid> AddWarehouse(FrontWareHouse warehouse);
        Task<FrontWareHouse> GetWarehouseInfo(Guid id);
        Task<bool> DeleteWarehouse(Guid id);
        Task<bool> ChangeWarehouse(Guid id, FrontWareHouse newWarehouse);
    }
}