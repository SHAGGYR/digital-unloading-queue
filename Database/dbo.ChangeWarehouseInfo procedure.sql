SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.ChangeWarehouseInfo
	@Id uniqueidentifier,
	@Name varchar(255),
	@Location varchar(255),
	@DefaultWorkTimeStart datetime,
	@DefaultWorkTimeStop datetime
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE dbo.Warehouses SET Name = @Name, Location = @Location, DefaultWorkTimeStart = @DefaultWorkTimeStart, DefaultWorkTimeStop = @DefaultWorkTimeStop Where Id = @Id;
	SELECT @@ROWCOUNT;
END
GO