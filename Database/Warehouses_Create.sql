CREATE TABLE Warehouses (
	Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    Name varchar(255) NOT NULL,
    Location varchar(255) NOT NULL,
    DefaultWorkTimeStart datetime NOT NULL,
	DefaultWorkTimeStop datetime NOT NULL
);