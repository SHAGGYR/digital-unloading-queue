﻿using System;
using DigitalUnloadingQueue.Contracts.Models;
using DigitalUnloadingQueue.Models;
using DBModels = DigitalUnloadingQueue.Contracts.Models;
using FrontModels = DigitalUnloadingQueue.Models;

namespace DigitalUnloadingQueue.Helpers
{
    public static class FrontToBackModelsMapper
    {
        public static Gate MapGateFrontToBack(GateInfo frontModel)
        {
            return new Gate()
            {
                Id = frontModel.GateId, WarehouseId = frontModel.WarehouseId, UniqueWorkTimeStart = frontModel.DateFrom,
                UniqueWorkTimeStop = frontModel.DateTo, AvailableLoadingTypes = frontModel.AvailableLoadingTypes,
                GateNumber = frontModel.GateNumber
            };
        }

        public static DBModels.LoadingOrder MapOrderFrontToBack(FrontModels.LoadingOrder frontModel)
        {
            return new DBModels.LoadingOrder()
            {
                Belts = frontModel.Belts,
                Dimensions = frontModel.Dimensions,
                Id = frontModel.Id,
                Pack = frontModel.Pack,
                Pieces = frontModel.Pieces,
                Volume = frontModel.Volume,
                Weight = frontModel.Weight,
                BodyType = frontModel.BodyType,
                DateAdded = DateTime.Now,
                DriverName = frontModel.DriverName,
                LoadName = frontModel.LoadName,
                PhoneNumber = frontModel.PhoneNumber,
                TruckId = frontModel.TruckId,
                UnloadingType = frontModel.UnloadingType,
                ADR = frontModel.ADR,
                ProcessTimeDuration = frontModel.ProcessTimeDuration
            };
        }

        public static DBModels.Warehouse MapWarehouseFrontToBack(FrontModels.Warehouse frontModel)
        {
            return new DBModels.Warehouse()
            {
                Location = frontModel.Location, Name = frontModel.Name,
                DefaultWorkTimeStart = frontModel.DefaultWorkTimeStart,
                DefaultWorkTimeStop = frontModel.DefaultWorkTimeStop
            };
        }
    }
}