﻿using System;

namespace DigitalUnloadingQueue.Models
{
    public abstract class Order
    {
        public  Guid Id { get; set; }
        public string DriverName { get; set; }
        public string PhoneNumber { get; set; }
        public string TruckId { get; set; }
    
        public string LoadName { get; set; }
        public string Weight { get; set; }
        public string Volume { get; set; }
        public string BodyType { get; set; }
        public string UnloadingType { get; set; }
        public string Pack { get; set; }
        public string Pieces { get; set; }
        public string Belts { get; set; }
        public string Dimensions { get; set; }
        public string ADR { get; set; }

        public int ProcessTimeDuration { get; set; }
    }
}