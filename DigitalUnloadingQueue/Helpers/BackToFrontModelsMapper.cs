﻿using DigitalUnloadingQueue.Contracts.Models;
using DigitalUnloadingQueue.Models;
using DBModels = DigitalUnloadingQueue.Contracts.Models;
using FrontModels = DigitalUnloadingQueue.Models;

namespace DigitalUnloadingQueue.Helpers
{
    public static class BackToFrontModelsMapper
    {
        public static GateInfo MapGateBackToFront(Gate dbGate)
        {
            return new GateInfo()
            {
                DateFrom = dbGate.UniqueWorkTimeStart, DateTo = dbGate.UniqueWorkTimeStop, GateId = dbGate.Id,
                GateNumber = dbGate.GateNumber, WarehouseId = dbGate.WarehouseId,
                AvailableLoadingTypes = dbGate.AvailableLoadingTypes
            };
        }

        public static FrontModels.LoadingOrder MapOrderBackToFront(DBModels.LoadingOrder dbOrder)
        {
            return new FrontModels.LoadingOrder
            {
                Belts = dbOrder.Belts,
                Dimensions = dbOrder.Dimensions,
                Id = dbOrder.Id,
                Pack = dbOrder.Pack,
                TruckId = dbOrder.TruckId,
                Pieces = dbOrder.Pieces,
                Volume = dbOrder.Volume,
                Weight = dbOrder.Weight,
                BodyType = dbOrder.BodyType,
                DriverName = dbOrder.DriverName,
                LoadName = dbOrder.LoadName,
                PhoneNumber = dbOrder.PhoneNumber,
                UnloadingType = dbOrder.UnloadingType,
                ADR = dbOrder.ADR,
                ProcessTimeDuration = dbOrder.ProcessTimeDuration
            };
        }

        public static FrontModels.Warehouse MapWarehouseBackToFront(DBModels.Warehouse dbModel)
        {
            return new FrontModels.Warehouse
            {
                Id = dbModel.Id,
                Location = dbModel.Location,
                Name = dbModel.Location,
                DefaultWorkTimeStart = dbModel.DefaultWorkTimeStart,
                DefaultWorkTimeStop = dbModel.DefaultWorkTimeStop
            };
        }

        public static FrontModels.PinnedOrder MapPinnedOrderBackToFront(DBModels.PinnedOrder dbModel)
        {
            return new FrontModels.PinnedOrder
            {
                Belts = dbModel.Belts, Dimensions = dbModel.Dimensions, Id = dbModel.Id, Pack = dbModel.Pack,
                Pieces = dbModel.Pieces, Volume = dbModel.Volume, Weight = dbModel.Weight, BodyType = dbModel.BodyType,
                CurrentState = dbModel.CurrentState, DriverName = dbModel.DriverName, GateId = dbModel.GateId,
                LoadName = dbModel.LoadName, PhoneNumber = dbModel.PhoneNumber, TruckId = dbModel.TruckId,
                UnloadingType = dbModel.UnloadingType, ADR = dbModel.ADR,
                ProcessTimeDuration = dbModel.ProcessTimeDuration, ProcessTimeStart = dbModel.ProcessTimeStart,
                ProcessTimeStop = dbModel.ProcessTimeStop
            };
        }
    }
}