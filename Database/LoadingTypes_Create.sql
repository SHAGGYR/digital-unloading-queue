CREATE TABLE LoadingTypes (
	Id INT PRIMARY KEY,
    Name varchar(255) NOT NULL,
    ProcessTimeHours int NOT NULL
);