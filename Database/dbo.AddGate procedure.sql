SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.AddGate
	@WarehouseId varchar(255),
	@UniqueWorkTimeStart datetime,
	@UniqueWorkTimeStop datetime,
	@AvailableLoadingTypes int,
	@GateNumber int
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.Gates(WarehouseId, UniqueWorkTimeStart, UniqueWorkTimeStop, AvailableLoadingTypes, GateNumber)
	OUTPUT INSERTED.Id
	VALUES (@WarehouseId, @UniqueWorkTimeStart, @UniqueWorkTimeStop, @AvailableLoadingTypes, @GateNumber)
END
GO