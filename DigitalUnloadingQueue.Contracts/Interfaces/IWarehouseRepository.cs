﻿using System;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.Contracts.Interfaces
{
    public interface IWarehouseRepository
    {
        Task<Guid> AddWarehouse(Warehouse warehouse);
        Task<Guid> DeleteWarehouse(Guid id);
        Task<int> ChangeWarehouse(Guid id, Warehouse newWarehouse);
        Task<Warehouse> GetWarehouseInfo(Guid id);
    }
}