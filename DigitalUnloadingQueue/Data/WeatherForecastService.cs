using System;
using System.Linq;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Interfaces;
using DigitalUnloadingQueue.Models;
using LoadingOrder = DigitalUnloadingQueue.Models.LoadingOrder;

namespace DigitalUnloadingQueue.Data
{
    public class WeatherForecastService
    {
//        для тестирования
        private readonly IWarehouseService _warehouseService;
        private readonly IGatesInfoService _gateService;
        private readonly ILoadingQueueService _ordersService;

        public WeatherForecastService(IWarehouseService warehouseService,
            ILoadingQueueService ordersService,
            IGatesInfoService gateService)
        {
            _warehouseService = warehouseService;
            _ordersService = ordersService;
            _gateService = gateService;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public Task<WeatherForecast[]> GetForecastAsync(DateTime startDate)
        {
            var rng = new Random();
            return Task.FromResult(Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = startDate.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToArray());
        }

        public async Task AddLoadAsync(LoadingOrder model)
        {
            var result = await _ordersService.AddOrder(model);                
            Console.WriteLine(result);
        }
    }
}