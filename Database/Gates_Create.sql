CREATE TABLE Gates (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	GateNumber int NOT NULL,
    WarehouseId UNIQUEIDENTIFIER NOT NULL,
	UniqueWorkTimeStart datetime,
	UniqueWorkTimeStop datetime,
    AvailableLoadingTypes int
);