SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.GetWarehouseInfo
	@Id uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, Name, Location, DefaultWorkTimeStart, DefaultWorkTimeStop FROM dbo.Warehouses Where Id = @Id;
END
GO