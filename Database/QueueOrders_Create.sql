CREATE TABLE QueueOrders (
    Id UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	DriverName Varchar(255) NOT NULL,
	PhoneNumber Varchar(255) NOT NULL,
	LoadName Varchar(255),
	Weight Varchar(255),
	Volume Varchar(255),
	BodyType Varchar(255),
	UnloadingType Varchar(255) NOT NULL,
	Pack Varchar(255),
	Pieces Varchar(255),
	Belts Varchar(255),
	Dimensions Varchar(255),
	ADR Varchar(255),
	DateAdded datetime NOT NULL
);