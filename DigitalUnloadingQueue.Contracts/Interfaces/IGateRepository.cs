﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.Contracts.Interfaces
{
    public interface IGateRepository
    {
        Task<Guid> AddGate(Gate gate);
        Task<Guid> DeleteGate(Guid gateId);
        Task<Gate> GetGateInfo(Guid gateId);
        Task<IEnumerable<Gate>> GetAllGatesInfo();
        Task<int> ChangeGateInfo(Guid gateId, Gate newGate);
        Task<IEnumerable<PinnedOrder>> GetAllPinnedOrders(Guid gateId);

    }
}