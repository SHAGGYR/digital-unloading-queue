SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.AddQueueOrder
	@DriverName varchar(255),
	@PhoneNumber varchar(255),
	@TruckId varchar(255),
	@LoadName varchar(255),
	@Weight varchar(255),
	@Volume varchar(255),
	@BodyType varchar(255),
	@UnloadingType varchar(255),
	@Pack varchar(255),
	@Pieces varchar(255),
	@Belts varchar(255),
	@Dimensions varchar(255),
	@ADR varchar(255),
	@DateAdded datetime,
	@ProcessTimeDuration int
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO Hackaton.dbo.QueueOrders(DriverName, PhoneNumber, TruckId, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, ProcessTimeDuration)
	OUTPUT INSERTED.Id
	VALUES (@DriverName, @PhoneNumber, @TruckId, @LoadName, @Weight, @Volume, @BodyType, @UnloadingType, @Pack, @Pieces, @Belts, @Dimensions, @ADR, @DateAdded, @ProcessTimeDuration)
END
GO