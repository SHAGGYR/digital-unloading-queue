﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Models;
using DigitalUnloadingQueue.Interfaces;
using DigitalUnloadingQueue.Helpers;

namespace DigitalUnloadingQueue.Data
{
    public class GatesInfosInfoService: IGatesInfoService
    {
        private readonly IGateRepository _gateRepository;

        public GatesInfosInfoService(IGateRepository gateRepository)
        {
            _gateRepository = gateRepository;
        }
        
        public async Task<List<GateInfo>> GetItemsAsync()
        {
            var rng = new Random();
            return await Task.FromResult(Enumerable.Range(1, 5).Select(index => new GateInfo
            {
                GateId = Guid.NewGuid(),
                GateNumber = index,
                DateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 5 + rng.Next(-5, 5), 0, 0),
                DateTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 20 + rng.Next(-3, 3), 0, 0)
            }).ToList());
        }

        public async Task SaveGateTimeAsync(Guid gateId, int dateTime)
        {
            await Task.Delay(300);
        }

        public async Task<Guid> AddGate(GateInfo gate)
        {
            if (gate?.WarehouseId == Guid.Empty)
            {
                throw new Exception("Warehouse id must be filled");
            }
            return await _gateRepository.AddGate(FrontToBackModelsMapper.MapGateFrontToBack(gate));
        }

        public async Task<bool> DeleteGate(Guid gateId)
        {
            if (gateId == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            var deletedGateId = await _gateRepository.DeleteGate(gateId);
            return !deletedGateId.Equals(Guid.Empty);
        }

        public async Task<GateInfo> GetGateInfo(Guid gateId)
        {
            if (gateId == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            var dbGateModel = await _gateRepository.GetGateInfo(gateId);
            return BackToFrontModelsMapper.MapGateBackToFront(dbGateModel);
        }

        public async Task<List<GateInfo>> GetAllGatesInfo()
        {
            var dbResult = await _gateRepository.GetAllGatesInfo();
            
            var result = dbResult.Select(BackToFrontModelsMapper.MapGateBackToFront).OrderBy(x => x.GateNumber).ToList();

            foreach (var gateInfo in result)
            {
                gateInfo.PinnedOrders = await GetAllPinnedOrders(gateInfo.GateId);
            }

            return result;
        }

        public async Task<List<PinnedOrder>> GetAllPinnedOrders(Guid gateId)
        {
            if (gateId == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            var result = await _gateRepository.GetAllPinnedOrders(gateId);
            return result.Select(BackToFrontModelsMapper.MapPinnedOrderBackToFront).ToList();
        }

        public async Task<bool> ChangeGateInfo(Guid gateId, GateInfo newFrontGate)
        {
            if (gateId == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            newFrontGate.GateId = gateId;
            var result = await _gateRepository.ChangeGateInfo(gateId, FrontToBackModelsMapper.MapGateFrontToBack(newFrontGate));
            return result == 1;
        }
    }
}