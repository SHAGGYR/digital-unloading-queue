﻿using System;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Contracts.Models;
using DigitalUnloadingQueue.Helpers;
using DigitalUnloadingQueue.Interfaces;
using FrontWarehouse = DigitalUnloadingQueue.Models.Warehouse;

namespace DigitalUnloadingQueue.Data
{
    public class WarehouseService: IWarehouseService
    {
        private readonly IWarehouseRepository _warehouseRepository;

        public WarehouseService(IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
        }
        
        public async Task<Guid> AddWarehouse(FrontWarehouse warehouse)
        {
            if (warehouse?.Name == null || warehouse.Location == null)
            {
                throw new Exception("Not all data filled. ");
            }

            return await _warehouseRepository.AddWarehouse(FrontToBackModelsMapper.MapWarehouseFrontToBack(warehouse));
        }

        public async Task<FrontWarehouse> GetWarehouseInfo(Guid id)
        {
            var result = await _warehouseRepository.GetWarehouseInfo(id);
            if (result == null)
            {
                throw new Exception("No such warehouse found.");
            }

            return BackToFrontModelsMapper.MapWarehouseBackToFront(result);
        }

        public async Task<bool> DeleteWarehouse(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            var deleteWarehouseId = await _warehouseRepository.DeleteWarehouse(id);
            return !deleteWarehouseId.Equals(Guid.Empty);
        }

        public async Task<bool> ChangeWarehouse(Guid id, FrontWarehouse newWarehouse)
        {
            if (id == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            newWarehouse.Id = id;
            var result = await _warehouseRepository.ChangeWarehouse(id, FrontToBackModelsMapper.MapWarehouseFrontToBack(newWarehouse));
            return result == 1;
        }
    }
}