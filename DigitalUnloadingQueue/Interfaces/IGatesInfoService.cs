﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Models;

namespace DigitalUnloadingQueue.Interfaces
{
    public interface IGatesInfoService
    {
        Task<Guid> AddGate(GateInfo gate);
        Task<bool> DeleteGate(Guid gateId);
        Task<GateInfo> GetGateInfo(Guid gateId);
        Task<List<GateInfo>> GetAllGatesInfo();
        Task<List<PinnedOrder>> GetAllPinnedOrders(Guid gateId);
        Task<bool> ChangeGateInfo(Guid gateId, GateInfo newFrontGate);
        Task SaveGateTimeAsync(Guid gateId, int dateTime);
    }
}