USE [Hackaton]
GO
/****** Object:  StoredProcedure [dbo].[DeleteGate]    Script Date: 12.12.2019 0:48:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[DeleteGate]
	@Id uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM Hackaton.dbo.Gates OUTPUT DELETED.Id WHERE Id = @Id;

	Declare @OrderIds nvarchar(max)
	
	SELECT @OrderIds = STRING_AGG (CAST (Id as nvarchar(max)), ',') FROM Hackaton.dbo.ProcessedOrders WHERE GateId = @Id
	SELECT @OrderIds;
	EXEC dbo.UnPinOrderToGate @OrderIds;

END
