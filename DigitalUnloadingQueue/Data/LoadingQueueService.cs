﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Helpers;
using DigitalUnloadingQueue.Interfaces;
using DBOrder = DigitalUnloadingQueue.Contracts.Models.LoadingOrder;
using FrontOrder = DigitalUnloadingQueue.Models.LoadingOrder;

namespace DigitalUnloadingQueue.Data
{
    public class LoadingQueueService: ILoadingQueueService
    {
        private readonly IOrderRepository _ordersRepository;

        public LoadingQueueService(IOrderRepository ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }
        
        public async Task<List<FrontOrder>> GetItemsAsync()
        {
            var items = await _ordersRepository.GetAllOrdersInQueue();
            return items.Select(BackToFrontModelsMapper.MapOrderBackToFront).ToList();
        }

        public async Task<Guid> AddOrder(FrontOrder frontOrder)
        {
            if (frontOrder?.PhoneNumber == null || frontOrder.DriverName == null || frontOrder.UnloadingType == null || frontOrder.TruckId == null)
            {
                throw new Exception("Not all required info filled");
            }

            return await _ordersRepository.AddOrder(FrontToBackModelsMapper.MapOrderFrontToBack(frontOrder));
        }

        public async Task ChangeOrderInfo(Guid orderId, FrontOrder frontOrder)
        {
            await _ordersRepository.ChangeOrderInfo(orderId, FrontToBackModelsMapper.MapOrderFrontToBack(frontOrder));
        }

        public async Task<bool> DeleteOrder(Guid orderId)
        {
            if (orderId == Guid.Empty)
            {
                throw new Exception("Empty id passed");
            }

            var deletedOrderId = await _ordersRepository.DeleteQueueOrder(orderId);
            return !deletedOrderId.Equals(Guid.Empty);
        }

        public async Task<bool> PinOrderToGate(Guid orderId, Guid gateId, DateTime startTime, DateTime endTime, int processDuration)
        {
            if (orderId == Guid.Empty || gateId == Guid.Empty || processDuration == 0)
            {
                throw new Exception("Some arguments are not valid.");
            }

            var result = await _ordersRepository.PinOrderToGate(orderId, gateId, startTime, endTime, processDuration);
            return result == 1;
        }

        public async Task<bool> UnpinOrderFromGate(Guid orderId)
        {
            if (orderId == Guid.Empty)
            {
                throw new Exception("Order id is null.");
            }

            var result = await _ordersRepository.UnpinOrderFromGate(orderId);
            return result != 0;
        }

        public async Task<FrontOrder> CheckOrderState(Guid orderId)
        {
            var result = await _ordersRepository.GetOrderInfo(orderId);
            if (result == null)
            {
                throw new Exception("No such order found.");
            }

            return BackToFrontModelsMapper.MapOrderBackToFront(result);
        }

        public Task<FrontOrder> RecoverOrderByTruckName(string truckId)
        {
            throw new NotImplementedException();
        }
    }
}