﻿using System;
using System.Threading.Tasks;
using Dapper;
using DigitalUnloadingQueue.Contracts.Interfaces;
using DigitalUnloadingQueue.Contracts.Models;

namespace DigitalUnloadingQueue.DataAccess.Repositories
{
    public class WarehouseRepository: IWarehouseRepository
    {
        private readonly BaseDataAccess _dataAccess;
        
        public WarehouseRepository(BaseDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        
        public Task<Guid> AddWarehouse(Warehouse warehouse)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Name", warehouse.Name);
            parameters.Add("@Location", warehouse.Location);
            parameters.Add("@DefaultWorkTimeStart", warehouse.DefaultWorkTimeStart);
            parameters.Add("@DefaultWorkTimeStop", warehouse.DefaultWorkTimeStop);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Guid>(StoredProcedures.AddWarehouse, parameters);
        }

        public Task<Guid> DeleteWarehouse(Guid id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
             return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Guid>(StoredProcedures.DeleteWarehouse, parameters);
        }

        public Task<int> ChangeWarehouse(Guid id, Warehouse newWarehouse)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            parameters.Add("@Name", newWarehouse.Name);
            parameters.Add("@Location", newWarehouse.Location);
            parameters.Add("@DefaultWorkTimeStart", newWarehouse.DefaultWorkTimeStart);
            parameters.Add("@DefaultWorkTimeStop", newWarehouse.DefaultWorkTimeStop);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<int>(StoredProcedures.ChangeWarehouseInfo, parameters);
        }

        public Task<Warehouse> GetWarehouseInfo(Guid id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            return _dataAccess.ExecuteStoredProcedureWhichReturnsSingleAsync<Warehouse>(StoredProcedures.GetWarehouseInfo, parameters);
        }
    }
}