SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.GetOrderInfo
	@Id uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Id, DriverName, PhoneNumber, TruckId, LoadName, Weight, Volume, BodyType, UnloadingType, Pack, Pieces, Belts, Dimensions, ADR, DateAdded, ProcessTimeDuration FROM Hackaton.dbo.QueueOrders Where Id = @Id;
END
GO